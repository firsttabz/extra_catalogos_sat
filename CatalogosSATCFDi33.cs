using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_Aduana
    public class C_Aduana
    {
        #region Member Variables
        protected int _idaduana;
        protected string _c_Aduana;
        protected string _Descripcion;
        #endregion
        #region Constructors
        public C_Aduana() { }
        public C_Aduana(string c_Aduana, string Descripcion)
        {
            this._c_Aduana=c_Aduana;
            this._Descripcion=Descripcion;
        }
        #endregion
        #region Public Properties
        public virtual int Idaduana
        {
            get {return _idaduana;}
            set {_idaduana=value;}
        }
        public virtual string C_Aduana
        {
            get {return _c_Aduana;}
            set {_c_Aduana=value;}
        }
        public virtual string Descripcion
        {
            get {return _Descripcion;}
            set {_Descripcion=value;}
        }
        #endregion
    }
    #endregion
}using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_ClaveProdServ
    public class C_ClaveProdServ
    {
        #region Member Variables
        protected int _idclaveprodserv;
        protected string _c_ClaveProdServ;
        protected string _Descripcion;
        protected unknown _FechaInicioVigencia;
        protected unknown _FechaFinVigencia;
        protected string _IncluirIVAtrasladado;
        protected string _IncluirIEPStrasladado;
        protected int _Complementoquedebeincluir;
        #endregion
        #region Constructors
        public C_ClaveProdServ() { }
        public C_ClaveProdServ(string c_ClaveProdServ, string Descripcion, unknown FechaInicioVigencia, unknown FechaFinVigencia, string IncluirIVAtrasladado, string IncluirIEPStrasladado, int Complementoquedebeincluir)
        {
            this._c_ClaveProdServ=c_ClaveProdServ;
            this._Descripcion=Descripcion;
            this._FechaInicioVigencia=FechaInicioVigencia;
            this._FechaFinVigencia=FechaFinVigencia;
            this._IncluirIVAtrasladado=IncluirIVAtrasladado;
            this._IncluirIEPStrasladado=IncluirIEPStrasladado;
            this._Complementoquedebeincluir=Complementoquedebeincluir;
        }
        #endregion
        #region Public Properties
        public virtual int Idclaveprodserv
        {
            get {return _idclaveprodserv;}
            set {_idclaveprodserv=value;}
        }
        public virtual string C_ClaveProdServ
        {
            get {return _c_ClaveProdServ;}
            set {_c_ClaveProdServ=value;}
        }
        public virtual string Descripcion
        {
            get {return _Descripcion;}
            set {_Descripcion=value;}
        }
        public virtual unknown FechaInicioVigencia
        {
            get {return _FechaInicioVigencia;}
            set {_FechaInicioVigencia=value;}
        }
        public virtual unknown FechaFinVigencia
        {
            get {return _FechaFinVigencia;}
            set {_FechaFinVigencia=value;}
        }
        public virtual string IncluirIVAtrasladado
        {
            get {return _IncluirIVAtrasladado;}
            set {_IncluirIVAtrasladado=value;}
        }
        public virtual string IncluirIEPStrasladado
        {
            get {return _IncluirIEPStrasladado;}
            set {_IncluirIEPStrasladado=value;}
        }
        public virtual int Complementoquedebeincluir
        {
            get {return _Complementoquedebeincluir;}
            set {_Complementoquedebeincluir=value;}
        }
        #endregion
    }
    #endregion
}using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_ClaveUnidad
    public class C_ClaveUnidad
    {
        #region Member Variables
        protected int _idClaveUnidad;
        protected string _ClaveUnidad;
        protected string _Nombre;
        protected string _Descripcion;
        protected unknown _Fechadeiniciodevigencia;
        protected unknown _Fechadefindevigencia;
        protected string _Simbolo;
        #endregion
        #region Constructors
        public C_ClaveUnidad() { }
        public C_ClaveUnidad(string ClaveUnidad, string Nombre, string Descripcion, unknown Fechadeiniciodevigencia, unknown Fechadefindevigencia, string Simbolo)
        {
            this._ClaveUnidad=ClaveUnidad;
            this._Nombre=Nombre;
            this._Descripcion=Descripcion;
            this._Fechadeiniciodevigencia=Fechadeiniciodevigencia;
            this._Fechadefindevigencia=Fechadefindevigencia;
            this._Simbolo=Simbolo;
        }
        #endregion
        #region Public Properties
        public virtual int IdClaveUnidad
        {
            get {return _idClaveUnidad;}
            set {_idClaveUnidad=value;}
        }
        public virtual string ClaveUnidad
        {
            get {return _ClaveUnidad;}
            set {_ClaveUnidad=value;}
        }
        public virtual string Nombre
        {
            get {return _Nombre;}
            set {_Nombre=value;}
        }
        public virtual string Descripcion
        {
            get {return _Descripcion;}
            set {_Descripcion=value;}
        }
        public virtual unknown Fechadeiniciodevigencia
        {
            get {return _Fechadeiniciodevigencia;}
            set {_Fechadeiniciodevigencia=value;}
        }
        public virtual unknown Fechadefindevigencia
        {
            get {return _Fechadefindevigencia;}
            set {_Fechadefindevigencia=value;}
        }
        public virtual string Simbolo
        {
            get {return _Simbolo;}
            set {_Simbolo=value;}
        }
        #endregion
    }
    #endregion
}using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_CodigoPostal
    public class C_CodigoPostal
    {
        #region Member Variables
        protected int _idCodPostal;
        protected string _c_CodigoPostal;
        protected string _c_Estado;
        protected string _c_Municipio;
        protected string _c_Localidad;
        #endregion
        #region Constructors
        public C_CodigoPostal() { }
        public C_CodigoPostal(string c_CodigoPostal, string c_Estado, string c_Municipio, string c_Localidad)
        {
            this._c_CodigoPostal=c_CodigoPostal;
            this._c_Estado=c_Estado;
            this._c_Municipio=c_Municipio;
            this._c_Localidad=c_Localidad;
        }
        #endregion
        #region Public Properties
        public virtual int IdCodPostal
        {
            get {return _idCodPostal;}
            set {_idCodPostal=value;}
        }
        public virtual string C_CodigoPostal
        {
            get {return _c_CodigoPostal;}
            set {_c_CodigoPostal=value;}
        }
        public virtual string C_Estado
        {
            get {return _c_Estado;}
            set {_c_Estado=value;}
        }
        public virtual string C_Municipio
        {
            get {return _c_Municipio;}
            set {_c_Municipio=value;}
        }
        public virtual string C_Localidad
        {
            get {return _c_Localidad;}
            set {_c_Localidad=value;}
        }
        #endregion
    }
    #endregion
}using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_FormaPago
    public class C_FormaPago
    {
        #region Member Variables
        protected int _idformadepago;
        protected string _c_FormaPago;
        protected string _Descripción;
        protected string _Bancarizado;
        protected string _NumerodeOperacion;
        protected string _RFC_del_Emisor_de_la_cuenta_ordenante;
        protected string _Cuenta_Ordenante;
        protected string _Patron_para_cuenta_ordenante;
        protected string _RFC_del_Emisor_Cuenta_de_Beneficiario;
        protected string _Cuenta_de_Benenficiario;
        protected string _Patron_para_cuenta_Beneficiaria;
        protected string _Tipo_Cadena_Pago;
        protected string _Nombre_del_Banco_emisor_de_la_cuenta_ordenante_en_caso_de_extran;
        #endregion
        #region Constructors
        public C_FormaPago() { }
        public C_FormaPago(string c_FormaPago, string Descripción, string Bancarizado, string NumerodeOperacion, string RFC_del_Emisor_de_la_cuenta_ordenante, string Cuenta_Ordenante, string Patron_para_cuenta_ordenante, string RFC_del_Emisor_Cuenta_de_Beneficiario, string Cuenta_de_Benenficiario, string Patron_para_cuenta_Beneficiaria, string Tipo_Cadena_Pago, string Nombre_del_Banco_emisor_de_la_cuenta_ordenante_en_caso_de_extran)
        {
            this._c_FormaPago=c_FormaPago;
            this._Descripción=Descripción;
            this._Bancarizado=Bancarizado;
            this._NumerodeOperacion=NumerodeOperacion;
            this._RFC_del_Emisor_de_la_cuenta_ordenante=RFC_del_Emisor_de_la_cuenta_ordenante;
            this._Cuenta_Ordenante=Cuenta_Ordenante;
            this._Patron_para_cuenta_ordenante=Patron_para_cuenta_ordenante;
            this._RFC_del_Emisor_Cuenta_de_Beneficiario=RFC_del_Emisor_Cuenta_de_Beneficiario;
            this._Cuenta_de_Benenficiario=Cuenta_de_Benenficiario;
            this._Patron_para_cuenta_Beneficiaria=Patron_para_cuenta_Beneficiaria;
            this._Tipo_Cadena_Pago=Tipo_Cadena_Pago;
            this._Nombre_del_Banco_emisor_de_la_cuenta_ordenante_en_caso_de_extran=Nombre_del_Banco_emisor_de_la_cuenta_ordenante_en_caso_de_extran;
        }
        #endregion
        #region Public Properties
        public virtual int Idformadepago
        {
            get {return _idformadepago;}
            set {_idformadepago=value;}
        }
        public virtual string C_FormaPago
        {
            get {return _c_FormaPago;}
            set {_c_FormaPago=value;}
        }
        public virtual string Descripción
        {
            get {return _Descripción;}
            set {_Descripción=value;}
        }
        public virtual string Bancarizado
        {
            get {return _Bancarizado;}
            set {_Bancarizado=value;}
        }
        public virtual string NumerodeOperacion
        {
            get {return _NumerodeOperacion;}
            set {_NumerodeOperacion=value;}
        }
        public virtual string RFC_del_Emisor_de_la_cuenta_ordenante
        {
            get {return _RFC_del_Emisor_de_la_cuenta_ordenante;}
            set {_RFC_del_Emisor_de_la_cuenta_ordenante=value;}
        }
        public virtual string Cuenta_Ordenante
        {
            get {return _Cuenta_Ordenante;}
            set {_Cuenta_Ordenante=value;}
        }
        public virtual string Patron_para_cuenta_ordenante
        {
            get {return _Patron_para_cuenta_ordenante;}
            set {_Patron_para_cuenta_ordenante=value;}
        }
        public virtual string RFC_del_Emisor_Cuenta_de_Beneficiario
        {
            get {return _RFC_del_Emisor_Cuenta_de_Beneficiario;}
            set {_RFC_del_Emisor_Cuenta_de_Beneficiario=value;}
        }
        public virtual string Cuenta_de_Benenficiario
        {
            get {return _Cuenta_de_Benenficiario;}
            set {_Cuenta_de_Benenficiario=value;}
        }
        public virtual string Patron_para_cuenta_Beneficiaria
        {
            get {return _Patron_para_cuenta_Beneficiaria;}
            set {_Patron_para_cuenta_Beneficiaria=value;}
        }
        public virtual string Tipo_Cadena_Pago
        {
            get {return _Tipo_Cadena_Pago;}
            set {_Tipo_Cadena_Pago=value;}
        }
        public virtual string Nombre_del_Banco_emisor_de_la_cuenta_ordenante_en_caso_de_extran
        {
            get {return _Nombre_del_Banco_emisor_de_la_cuenta_ordenante_en_caso_de_extran;}
            set {_Nombre_del_Banco_emisor_de_la_cuenta_ordenante_en_caso_de_extran=value;}
        }
        #endregion
    }
    #endregion
}using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_Impuesto
    public class C_Impuesto
    {
        #region Member Variables
        protected int _idimpuesto;
        protected string _c_Impuesto;
        protected string _Descripcion;
        protected string _Retencion;
        protected string _Traslado;
        protected string _Local_o_federal;
        protected string _Entidad_en_la_que_aplica;
        #endregion
        #region Constructors
        public C_Impuesto() { }
        public C_Impuesto(string c_Impuesto, string Descripcion, string Retencion, string Traslado, string Local_o_federal, string Entidad_en_la_que_aplica)
        {
            this._c_Impuesto=c_Impuesto;
            this._Descripcion=Descripcion;
            this._Retencion=Retencion;
            this._Traslado=Traslado;
            this._Local_o_federal=Local_o_federal;
            this._Entidad_en_la_que_aplica=Entidad_en_la_que_aplica;
        }
        #endregion
        #region Public Properties
        public virtual int Idimpuesto
        {
            get {return _idimpuesto;}
            set {_idimpuesto=value;}
        }
        public virtual string C_Impuesto
        {
            get {return _c_Impuesto;}
            set {_c_Impuesto=value;}
        }
        public virtual string Descripcion
        {
            get {return _Descripcion;}
            set {_Descripcion=value;}
        }
        public virtual string Retencion
        {
            get {return _Retencion;}
            set {_Retencion=value;}
        }
        public virtual string Traslado
        {
            get {return _Traslado;}
            set {_Traslado=value;}
        }
        public virtual string Local_o_federal
        {
            get {return _Local_o_federal;}
            set {_Local_o_federal=value;}
        }
        public virtual string Entidad_en_la_que_aplica
        {
            get {return _Entidad_en_la_que_aplica;}
            set {_Entidad_en_la_que_aplica=value;}
        }
        #endregion
    }
    #endregion
}using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_MetodoPago
    public class C_MetodoPago
    {
        #region Member Variables
        protected int _idmetodopago;
        protected string _c_MetodoPago;
        protected string _Descripción;
        #endregion
        #region Constructors
        public C_MetodoPago() { }
        public C_MetodoPago(string c_MetodoPago, string Descripción)
        {
            this._c_MetodoPago=c_MetodoPago;
            this._Descripción=Descripción;
        }
        #endregion
        #region Public Properties
        public virtual int Idmetodopago
        {
            get {return _idmetodopago;}
            set {_idmetodopago=value;}
        }
        public virtual string C_MetodoPago
        {
            get {return _c_MetodoPago;}
            set {_c_MetodoPago=value;}
        }
        public virtual string Descripción
        {
            get {return _Descripción;}
            set {_Descripción=value;}
        }
        #endregion
    }
    #endregion
}using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_Moneda
    public class C_Moneda
    {
        #region Member Variables
        protected int _idmoneda;
        protected string _c_Moneda;
        protected string _Descripcion;
        protected string _Decimales;
        protected string _Porcentaje_variacion;
        #endregion
        #region Constructors
        public C_Moneda() { }
        public C_Moneda(string c_Moneda, string Descripcion, string Decimales, string Porcentaje_variacion)
        {
            this._c_Moneda=c_Moneda;
            this._Descripcion=Descripcion;
            this._Decimales=Decimales;
            this._Porcentaje_variacion=Porcentaje_variacion;
        }
        #endregion
        #region Public Properties
        public virtual int Idmoneda
        {
            get {return _idmoneda;}
            set {_idmoneda=value;}
        }
        public virtual string C_Moneda
        {
            get {return _c_Moneda;}
            set {_c_Moneda=value;}
        }
        public virtual string Descripcion
        {
            get {return _Descripcion;}
            set {_Descripcion=value;}
        }
        public virtual string Decimales
        {
            get {return _Decimales;}
            set {_Decimales=value;}
        }
        public virtual string Porcentaje_variacion
        {
            get {return _Porcentaje_variacion;}
            set {_Porcentaje_variacion=value;}
        }
        #endregion
    }
    #endregion
}using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_NumPedimentoAduana
    public class C_NumPedimentoAduana
    {
        #region Member Variables
        protected int _idNumPedimentoAduana;
        protected string _c_Aduana;
        protected string _Patente;
        protected string _Ejercicio;
        protected string _Cantidad;
        protected unknown _Fecha_inicio_de_vigencia;
        protected unknown _Fecha_fin_de_vigencia;
        #endregion
        #region Constructors
        public C_NumPedimentoAduana() { }
        public C_NumPedimentoAduana(string c_Aduana, string Patente, string Ejercicio, string Cantidad, unknown Fecha_inicio_de_vigencia, unknown Fecha_fin_de_vigencia)
        {
            this._c_Aduana=c_Aduana;
            this._Patente=Patente;
            this._Ejercicio=Ejercicio;
            this._Cantidad=Cantidad;
            this._Fecha_inicio_de_vigencia=Fecha_inicio_de_vigencia;
            this._Fecha_fin_de_vigencia=Fecha_fin_de_vigencia;
        }
        #endregion
        #region Public Properties
        public virtual int IdNumPedimentoAduana
        {
            get {return _idNumPedimentoAduana;}
            set {_idNumPedimentoAduana=value;}
        }
        public virtual string C_Aduana
        {
            get {return _c_Aduana;}
            set {_c_Aduana=value;}
        }
        public virtual string Patente
        {
            get {return _Patente;}
            set {_Patente=value;}
        }
        public virtual string Ejercicio
        {
            get {return _Ejercicio;}
            set {_Ejercicio=value;}
        }
        public virtual string Cantidad
        {
            get {return _Cantidad;}
            set {_Cantidad=value;}
        }
        public virtual unknown Fecha_inicio_de_vigencia
        {
            get {return _Fecha_inicio_de_vigencia;}
            set {_Fecha_inicio_de_vigencia=value;}
        }
        public virtual unknown Fecha_fin_de_vigencia
        {
            get {return _Fecha_fin_de_vigencia;}
            set {_Fecha_fin_de_vigencia=value;}
        }
        #endregion
    }
    #endregion
}using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_Pais
    public class C_Pais
    {
        #region Member Variables
        protected int _idpais;
        protected string _c_Pais;
        protected string _Descripcion;
        protected string _Formato_de_codigo_postal;
        protected string _Formato_de_Registro_de_Identidad_Tributaria;
        protected string _Validacion_del_Registro_de_Identidad_Tributaria;
        protected string _Agrupaciones;
        #endregion
        #region Constructors
        public C_Pais() { }
        public C_Pais(string c_Pais, string Descripcion, string Formato_de_codigo_postal, string Formato_de_Registro_de_Identidad_Tributaria, string Validacion_del_Registro_de_Identidad_Tributaria, string Agrupaciones)
        {
            this._c_Pais=c_Pais;
            this._Descripcion=Descripcion;
            this._Formato_de_codigo_postal=Formato_de_codigo_postal;
            this._Formato_de_Registro_de_Identidad_Tributaria=Formato_de_Registro_de_Identidad_Tributaria;
            this._Validacion_del_Registro_de_Identidad_Tributaria=Validacion_del_Registro_de_Identidad_Tributaria;
            this._Agrupaciones=Agrupaciones;
        }
        #endregion
        #region Public Properties
        public virtual int Idpais
        {
            get {return _idpais;}
            set {_idpais=value;}
        }
        public virtual string C_Pais
        {
            get {return _c_Pais;}
            set {_c_Pais=value;}
        }
        public virtual string Descripcion
        {
            get {return _Descripcion;}
            set {_Descripcion=value;}
        }
        public virtual string Formato_de_codigo_postal
        {
            get {return _Formato_de_codigo_postal;}
            set {_Formato_de_codigo_postal=value;}
        }
        public virtual string Formato_de_Registro_de_Identidad_Tributaria
        {
            get {return _Formato_de_Registro_de_Identidad_Tributaria;}
            set {_Formato_de_Registro_de_Identidad_Tributaria=value;}
        }
        public virtual string Validacion_del_Registro_de_Identidad_Tributaria
        {
            get {return _Validacion_del_Registro_de_Identidad_Tributaria;}
            set {_Validacion_del_Registro_de_Identidad_Tributaria=value;}
        }
        public virtual string Agrupaciones
        {
            get {return _Agrupaciones;}
            set {_Agrupaciones=value;}
        }
        #endregion
    }
    #endregion
}using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_PatenteAduanal
    public class C_PatenteAduanal
    {
        #region Member Variables
        protected int _idpatenteaduanal;
        protected string _C_PatenteAduanal;
        protected unknown _Inicio_de_vigencia;
        protected unknown _Fin_de_vigencia;
        #endregion
        #region Constructors
        public C_PatenteAduanal() { }
        public C_PatenteAduanal(string C_PatenteAduanal, unknown Inicio_de_vigencia, unknown Fin_de_vigencia)
        {
            this._C_PatenteAduanal=C_PatenteAduanal;
            this._Inicio_de_vigencia=Inicio_de_vigencia;
            this._Fin_de_vigencia=Fin_de_vigencia;
        }
        #endregion
        #region Public Properties
        public virtual int Idpatenteaduanal
        {
            get {return _idpatenteaduanal;}
            set {_idpatenteaduanal=value;}
        }
        public virtual string C_PatenteAduanal
        {
            get {return _C_PatenteAduanal;}
            set {_C_PatenteAduanal=value;}
        }
        public virtual unknown Inicio_de_vigencia
        {
            get {return _Inicio_de_vigencia;}
            set {_Inicio_de_vigencia=value;}
        }
        public virtual unknown Fin_de_vigencia
        {
            get {return _Fin_de_vigencia;}
            set {_Fin_de_vigencia=value;}
        }
        #endregion
    }
    #endregion
}using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_RegimenFiscal
    public class C_RegimenFiscal
    {
        #region Member Variables
        protected int _idregimenfiscal;
        protected string _c_RegimenFiscal;
        protected string _Descripcion;
        protected string _Fisica;
        protected string _Moral;
        protected unknown _Fecha_inicio_de_vigencia;
        protected unknown _Fecha_fin_de_vigencia;
        #endregion
        #region Constructors
        public C_RegimenFiscal() { }
        public C_RegimenFiscal(string c_RegimenFiscal, string Descripcion, string Fisica, string Moral, unknown Fecha_inicio_de_vigencia, unknown Fecha_fin_de_vigencia)
        {
            this._c_RegimenFiscal=c_RegimenFiscal;
            this._Descripcion=Descripcion;
            this._Fisica=Fisica;
            this._Moral=Moral;
            this._Fecha_inicio_de_vigencia=Fecha_inicio_de_vigencia;
            this._Fecha_fin_de_vigencia=Fecha_fin_de_vigencia;
        }
        #endregion
        #region Public Properties
        public virtual int Idregimenfiscal
        {
            get {return _idregimenfiscal;}
            set {_idregimenfiscal=value;}
        }
        public virtual string C_RegimenFiscal
        {
            get {return _c_RegimenFiscal;}
            set {_c_RegimenFiscal=value;}
        }
        public virtual string Descripcion
        {
            get {return _Descripcion;}
            set {_Descripcion=value;}
        }
        public virtual string Fisica
        {
            get {return _Fisica;}
            set {_Fisica=value;}
        }
        public virtual string Moral
        {
            get {return _Moral;}
            set {_Moral=value;}
        }
        public virtual unknown Fecha_inicio_de_vigencia
        {
            get {return _Fecha_inicio_de_vigencia;}
            set {_Fecha_inicio_de_vigencia=value;}
        }
        public virtual unknown Fecha_fin_de_vigencia
        {
            get {return _Fecha_fin_de_vigencia;}
            set {_Fecha_fin_de_vigencia=value;}
        }
        #endregion
    }
    #endregion
}using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_TasaOCuota
    public class C_TasaOCuota
    {
        #region Member Variables
        protected int _idtasaocuota;
        protected string _Rango_o_Fijo;
        protected string _c_TasaOCuota_Valor_Minimo;
        protected string _c_TasaOCuota_Valor_Maximo;
        protected string _Impuesto;
        protected string _Factor;
        protected string _Traslado;
        protected string _Retencion;
        protected unknown _Fecha_inicio_de_vigencia;
        protected unknown _Fecha_fin_de_vigencia;
        #endregion
        #region Constructors
        public C_TasaOCuota() { }
        public C_TasaOCuota(string Rango_o_Fijo, string c_TasaOCuota_Valor_Minimo, string c_TasaOCuota_Valor_Maximo, string Impuesto, string Factor, string Traslado, string Retencion, unknown Fecha_inicio_de_vigencia, unknown Fecha_fin_de_vigencia)
        {
            this._Rango_o_Fijo=Rango_o_Fijo;
            this._c_TasaOCuota_Valor_Minimo=c_TasaOCuota_Valor_Minimo;
            this._c_TasaOCuota_Valor_Maximo=c_TasaOCuota_Valor_Maximo;
            this._Impuesto=Impuesto;
            this._Factor=Factor;
            this._Traslado=Traslado;
            this._Retencion=Retencion;
            this._Fecha_inicio_de_vigencia=Fecha_inicio_de_vigencia;
            this._Fecha_fin_de_vigencia=Fecha_fin_de_vigencia;
        }
        #endregion
        #region Public Properties
        public virtual int Idtasaocuota
        {
            get {return _idtasaocuota;}
            set {_idtasaocuota=value;}
        }
        public virtual string Rango_o_Fijo
        {
            get {return _Rango_o_Fijo;}
            set {_Rango_o_Fijo=value;}
        }
        public virtual string C_TasaOCuota_Valor_Minimo
        {
            get {return _c_TasaOCuota_Valor_Minimo;}
            set {_c_TasaOCuota_Valor_Minimo=value;}
        }
        public virtual string C_TasaOCuota_Valor_Maximo
        {
            get {return _c_TasaOCuota_Valor_Maximo;}
            set {_c_TasaOCuota_Valor_Maximo=value;}
        }
        public virtual string Impuesto
        {
            get {return _Impuesto;}
            set {_Impuesto=value;}
        }
        public virtual string Factor
        {
            get {return _Factor;}
            set {_Factor=value;}
        }
        public virtual string Traslado
        {
            get {return _Traslado;}
            set {_Traslado=value;}
        }
        public virtual string Retencion
        {
            get {return _Retencion;}
            set {_Retencion=value;}
        }
        public virtual unknown Fecha_inicio_de_vigencia
        {
            get {return _Fecha_inicio_de_vigencia;}
            set {_Fecha_inicio_de_vigencia=value;}
        }
        public virtual unknown Fecha_fin_de_vigencia
        {
            get {return _Fecha_fin_de_vigencia;}
            set {_Fecha_fin_de_vigencia=value;}
        }
        #endregion
    }
    #endregion
}using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_TipoDeComprobante
    public class C_TipoDeComprobante
    {
        #region Member Variables
        protected int _idtipocomprobante;
        protected string _c_TipoDeComprobante;
        protected string _Descripcion;
        protected string _Valor_maximo;
        protected string _Valor_maximo_NS;
        protected string _Valor_maximo_NdS;
        protected unknown _Fecha_inicio_de_vigencia;
        protected unknown _Fecha_fin_de_vigencia;
        #endregion
        #region Constructors
        public C_TipoDeComprobante() { }
        public C_TipoDeComprobante(string c_TipoDeComprobante, string Descripcion, string Valor_maximo, string Valor_maximo_NS, string Valor_maximo_NdS, unknown Fecha_inicio_de_vigencia, unknown Fecha_fin_de_vigencia)
        {
            this._c_TipoDeComprobante=c_TipoDeComprobante;
            this._Descripcion=Descripcion;
            this._Valor_maximo=Valor_maximo;
            this._Valor_maximo_NS=Valor_maximo_NS;
            this._Valor_maximo_NdS=Valor_maximo_NdS;
            this._Fecha_inicio_de_vigencia=Fecha_inicio_de_vigencia;
            this._Fecha_fin_de_vigencia=Fecha_fin_de_vigencia;
        }
        #endregion
        #region Public Properties
        public virtual int Idtipocomprobante
        {
            get {return _idtipocomprobante;}
            set {_idtipocomprobante=value;}
        }
        public virtual string C_TipoDeComprobante
        {
            get {return _c_TipoDeComprobante;}
            set {_c_TipoDeComprobante=value;}
        }
        public virtual string Descripcion
        {
            get {return _Descripcion;}
            set {_Descripcion=value;}
        }
        public virtual string Valor_maximo
        {
            get {return _Valor_maximo;}
            set {_Valor_maximo=value;}
        }
        public virtual string Valor_maximo_NS
        {
            get {return _Valor_maximo_NS;}
            set {_Valor_maximo_NS=value;}
        }
        public virtual string Valor_maximo_NdS
        {
            get {return _Valor_maximo_NdS;}
            set {_Valor_maximo_NdS=value;}
        }
        public virtual unknown Fecha_inicio_de_vigencia
        {
            get {return _Fecha_inicio_de_vigencia;}
            set {_Fecha_inicio_de_vigencia=value;}
        }
        public virtual unknown Fecha_fin_de_vigencia
        {
            get {return _Fecha_fin_de_vigencia;}
            set {_Fecha_fin_de_vigencia=value;}
        }
        #endregion
    }
    #endregion
}using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_TipoFactor
    public class C_TipoFactor
    {
        #region Member Variables
        protected int _idtipofactor;
        protected string _c_TipoFactor;
        #endregion
        #region Constructors
        public C_TipoFactor() { }
        public C_TipoFactor(string c_TipoFactor)
        {
            this._c_TipoFactor=c_TipoFactor;
        }
        #endregion
        #region Public Properties
        public virtual int Idtipofactor
        {
            get {return _idtipofactor;}
            set {_idtipofactor=value;}
        }
        public virtual string C_TipoFactor
        {
            get {return _c_TipoFactor;}
            set {_c_TipoFactor=value;}
        }
        #endregion
    }
    #endregion
}using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_TipoRelacion
    public class C_TipoRelacion
    {
        #region Member Variables
        protected int _idtiporelacion;
        protected string _c_TipoRelacion;
        protected string _Descripción;
        #endregion
        #region Constructors
        public C_TipoRelacion() { }
        public C_TipoRelacion(string c_TipoRelacion, string Descripción)
        {
            this._c_TipoRelacion=c_TipoRelacion;
            this._Descripción=Descripción;
        }
        #endregion
        #region Public Properties
        public virtual int Idtiporelacion
        {
            get {return _idtiporelacion;}
            set {_idtiporelacion=value;}
        }
        public virtual string C_TipoRelacion
        {
            get {return _c_TipoRelacion;}
            set {_c_TipoRelacion=value;}
        }
        public virtual string Descripción
        {
            get {return _Descripción;}
            set {_Descripción=value;}
        }
        #endregion
    }
    #endregion
}using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
namespace CatalogosSATCFDi
{
    #region C_UsoCFDI
    public class C_UsoCFDI
    {
        #region Member Variables
        protected int _idusocfdi;
        protected string _c_UsoCFDI;
        protected string _Descripcion;
        protected string _Fisica;
        protected string _Moral;
        protected unknown _Fecha_inicio_de_vigencia;
        protected unknown _Fecha_fin_de_vigencia;
        #endregion
        #region Constructors
        public C_UsoCFDI() { }
        public C_UsoCFDI(string c_UsoCFDI, string Descripcion, string Fisica, string Moral, unknown Fecha_inicio_de_vigencia, unknown Fecha_fin_de_vigencia)
        {
            this._c_UsoCFDI=c_UsoCFDI;
            this._Descripcion=Descripcion;
            this._Fisica=Fisica;
            this._Moral=Moral;
            this._Fecha_inicio_de_vigencia=Fecha_inicio_de_vigencia;
            this._Fecha_fin_de_vigencia=Fecha_fin_de_vigencia;
        }
        #endregion
        #region Public Properties
        public virtual int Idusocfdi
        {
            get {return _idusocfdi;}
            set {_idusocfdi=value;}
        }
        public virtual string C_UsoCFDI
        {
            get {return _c_UsoCFDI;}
            set {_c_UsoCFDI=value;}
        }
        public virtual string Descripcion
        {
            get {return _Descripcion;}
            set {_Descripcion=value;}
        }
        public virtual string Fisica
        {
            get {return _Fisica;}
            set {_Fisica=value;}
        }
        public virtual string Moral
        {
            get {return _Moral;}
            set {_Moral=value;}
        }
        public virtual unknown Fecha_inicio_de_vigencia
        {
            get {return _Fecha_inicio_de_vigencia;}
            set {_Fecha_inicio_de_vigencia=value;}
        }
        public virtual unknown Fecha_fin_de_vigencia
        {
            get {return _Fecha_fin_de_vigencia;}
            set {_Fecha_fin_de_vigencia=value;}
        }
        #endregion
    }
    #endregion
}